from dictionnaire import villes
from time import *

def trajet(villeDebut, villeFin):
    captalizedVilleDebut = villeDebut.lower().capitalize()
    captalizedVilleFin = villeFin.lower().capitalize()
    if captalizedVilleDebut != captalizedVilleFin:
        if captalizedVilleDebut in villes:
            if captalizedVilleFin in villes[captalizedVilleDebut]:
                distance = villes[captalizedVilleDebut][captalizedVilleFin] * 1000
                distanceParcourue = 0
                vitesse = 0
                tempsEnSecondes = 0
                tempsAvantPause = 0
                doitFaireUnePause = False
                pauses = 0
                distanceDeFreinage = 13000
                while (distanceParcourue < distance):

                    if (tempsAvantPause == 6660):
                        doitFaireUnePause = True
                    if(doitFaireUnePause):
                        if (vitesse > 25/9):
                            vitesse -= 25 / 9
                            tempsEnSecondes += 60
                            distanceParcourue += vitesse * 60
                        elif (vitesse < 25/9):
                            tempsEnSecondes += 900
                            tempsAvantPause = 0
                            vitesse= 0
                            pauses += 1
                            doitFaireUnePause = False

                    else:
                        tempsEnSecondes += 60
                        tempsAvantPause += 60
                        distanceParcourue += vitesse * 60

                    if (tempsAvantPause < 600):
                        if (vitesse < 25):
                            vitesse += 25 / 9
                            distanceParcourue += vitesse * 60

                    
                    distanceRestante = (distance - distanceParcourue)
                    while ( distanceRestante <= 6500 ):
                        tempsAvantPause += 20000
                        distanceRestante = (distance - distanceParcourue)
                        if (vitesse >= 25 / 9):
                            vitesse -= 25 / 9
                            tempsEnSecondes += 60
                            distanceParcourue += vitesse * 60
                        elif (vitesse < 25/9):
                            vitesse = 0
                            print(" Ville de départ : {}       |        Ville d'arrivée : {}       |            Durée totale: {}          ".format(captalizedVilleDebut, captalizedVilleFin, strftime('%H:%M', gmtime(tempsEnSecondes))))
                            return tempsEnSecondes
            else:
                print("Le trajet n'est pas connu.")
        else:
            print("Cette ville ne peut pas être utilisé comme ville de départ.")
    else:
        print("Tu es déjà arrivé, c'est la même ville.")



if __name__ == "__main__":
    villeDebut = input("Quelle est votre ville de départ ?\n")
    villeFin = input("Quelle est votre ville d'arrivée ?\n")
    trajet(villeDebut, villeFin)

